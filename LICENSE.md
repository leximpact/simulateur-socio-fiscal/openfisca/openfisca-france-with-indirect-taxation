# OpenFisca France with Indirect Taxation

_Fusion des systèmes socio-fiscaux OpenFisca-France & OpenFisca-France-indirect-taxation_

By [Emmanuel Raviard](mailto:emmanuel.raviart@assemblee-nationale.fr),
By [Benoît Courty](mailto:benoit.courty@assemblee-nationale.fr),

Copyright (C) 2022, 2023 Assemblée nationale

https://git.leximpact.dev/leximpact/simulateur-socio-fiscal/openfisca/openfisca-france-with-indirect-taxation/

> OpenFisca France with Indirect Taxation LexImpact is free software; you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
>
> OpenFisca France with Indirect Taxation LexImpact is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see <http://www.gnu.org/licenses/>.
