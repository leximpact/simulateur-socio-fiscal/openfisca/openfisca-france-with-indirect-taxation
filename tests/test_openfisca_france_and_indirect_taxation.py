from openfisca_france_with_indirect_taxation import __version__, CountryTaxBenefitSystem


def test_version():
    assert __version__ == '0.1.0'


def test_create_tax_benefit_system():
    tax_benefit_system = CountryTaxBenefitSystem()
