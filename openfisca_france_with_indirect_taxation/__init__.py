__version__ = "0.1.0"

from openfisca_core.taxbenefitsystems import TaxBenefitSystem

from openfisca_france import CountryTaxBenefitSystem as FranceTaxBenefitSystem
from openfisca_france_indirect_taxation import (
    CountryTaxBenefitSystem as IndirectTaxationTaxBenefitSystem,
)


class CountryTaxBenefitSystem(TaxBenefitSystem):
    """French tax benefit system merged with French Indirect Taxation tax benefit system"""

    CURRENCY = '€'

    def __init__(self):
        france_tax_benefit_system = FranceTaxBenefitSystem()
        indirect_taxation_tax_benefit_system = IndirectTaxationTaxBenefitSystem()
        TaxBenefitSystem.__init__(self, france_tax_benefit_system.entities)

        self.parameters = france_tax_benefit_system.parameters
        for (
            name,
            parameter,
        ) in indirect_taxation_tax_benefit_system.parameters.children.items():
            if name in self.parameters.children:
                print(
                    f'Skipping France indirect taxation parameter {name},',
                    'already defined in France',
                )
                continue
            self.parameters.add_child(name, parameter)

        self.variables = france_tax_benefit_system.variables
        for name, variable in indirect_taxation_tax_benefit_system.variables.items():
            if name in self.variables:
                print(
                    f'Skipping France indirect taxation variable {name},',
                    'already defined in France',
                )
                continue
            self.variables[name] = variable
