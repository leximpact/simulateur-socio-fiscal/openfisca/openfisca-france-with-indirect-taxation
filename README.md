Ce dépôt est une fusion des dépôts suivants https://github.com/openfisca/openfisca-france-indirect-taxation/tree/cas-type et https://github.com/openfisca/openfisca-france/tree/wip-leximpact


# OpenFisca France with Indirect Taxation

## _Fusion des systèmes socio-fiscaux OpenFisca-France & OpenFisca-France-indirect-taxation_

## Installation

```shell
git clone https://git.leximpact.dev/leximpact/openfisca-france-with-indirect-taxation.git
cd openfisca-france-with-indirect-taxation
poetry install
```

## Test

```shell
pytest
openfisca test --country-package openfisca_france_with_indirect_taxation openfisca_france_with_indirect_taxation/tests